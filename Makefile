#!/usr/bin/make -f

noncelens = 96 104 112 120
newvecs = $(foreach noncelen,$(noncelens),$(foreach test,1 2 3,test-vector-$(test)-nonce$(noncelen).txt))

all: $(newvecs)

check: test-vector-1.out test-vector-2.out test-vector-3.out
	diff -u test-vector-1.txt test-vector-1.out
	diff -u test-vector-2.txt test-vector-2.out
	diff -u test-vector-3.txt test-vector-3.out

test-vector-%.out: test-vector-%-nonce96.txt
	cp $< $@

test-vector-1-nonce%.txt: ocb_vectors.rb aes_alg.so
	./ocb_vectors.rb $* > $@

%: %.c
	gcc -Wall -Werror -pedantic -o $@ $< $(shell pkg-config --libs --cflags openssl)

test-vector-2-nonce%.txt: ocb_vectors_16_12_%
	./$< vector2 > $@

ocbvecs = $(foreach nonce,$(noncelens),$(foreach tag,16 12 8,$(foreach key,16 24 32,ocb_vectors_$(key)_$(tag)_$(nonce))))

test-vector-3-nonce%.txt: $(ocbvecs)
	printf 'Using %d bit nonce:\n' $* > $@.tmp
	$(foreach vec,$(filter %_$*,$(ocbvecs)),./$(vec) omnibus >> $@.tmp &&) true
	mv $@.tmp $@

empty:=
space:= $(empty) $(empty)
ocb_vectors_%: ocb_vectors.c
	gcc -Wall -Werror -pedantic \
	   -DKEYBYTES=$(word 1,$(subst _,$(space),$*)) \
	   -DTAGBYTES=$(word 2,$(subst _,$(space),$*)) \
	   -DNONCEBYTES=\($(word 3,$(subst _,$(space),$*))/8\) \
	-o $@ $< $(shell pkg-config --libs --cflags openssl)

aes_cons.h: ruby-aes-1.1/ext/ruby-aes/aes_gencons.rb
	ruby $<

aes_alg.so: ruby-aes-1.1/ext/ruby-aes/aes_alg.c aes_cons.h
	gcc -shared -Wall -Werror -fPIC -o aes_alg.so -I. $< $(shell pkg-config --cflags ruby)


# double-checking vector 1 C implementation against ruby implementation:
# doesn't succeed cleanly because ocb_vectors.c doesn't produce the internal state at the end of the dump

vec1alts = $(foreach noncelen,$(noncelens),test-vector-1-from-c-nonce$(noncelen).txt)

test-vector-1-from-c-nonce%.txt: ocb_vectors_16_16_%
	./$< > $@

check_more: $(vec1alts) $(newvecs)
	unset err; for x in $(noncelens); do printf "testing nonce %d\n" $$x; if ! diff -u test-vector-1-nonce$$x.txt test-vector-1-from-c-nonce$$x.txt; then err="$$err $$x"; fi; done; if [ -n "$err" ]; then exit 1; fi

## unused: ocb_refs:
ocbrefs = $(foreach tag,16 12 8,$(foreach key,16 24 32,ocb_ref_$(key)_$(tag)_12))
ocb_ref_%: ocb_ref.c
	gcc -Wall -Werror -pedantic \
	   -DKEYBYTES=$(word 1,$(subst _,$(space),$*)) \
	   -DTAGBYTES=$(word 2,$(subst _,$(space),$*)) \
	   -DNONCEBYTES=$(word 3,$(subst _,$(space),$*)) \
	-o $@ $< $(shell pkg-config --libs --cflags openssl)

clean:
	rm -f ocb_ref aes_alg.so aes_cons.h test-vector-*.out ocb_ref_* ocb_vectors_* test-vector-1-from-c-nonce*.txt *.tmp aes_alg.so aes_cons.h ocb_vectors *.out test-vector-*-nonce96.txt

.PHONY: clean all
.PRECIOUS: $(newvecs)
