# OCB Test Vector recreation

This repository shows my work recreating the test vectors from [Appendix A of RFC 7253](https://datatracker.ietf.org/doc/html/rfc7253#appendix-A).

The goal here is to recreate all the test vectors in Appendix A from source, and then parameterize the code so that we can re-create comparable vectors with a 15-byte (120 bit) nonce instead of a 12-byte (96-bit) nonce.

I'm interested in this because [OpenPGP aims to use 15-byte nonces](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/83), and we don't have good test vectors published for it.

## Original Source Code

This is done with the original code found on [Phil Rogaway's OCB page](https://www.cs.ucdavis.edu/~rogaway/ocb/news/), in particular the [unoptimized C source](https://www.cs.ucdavis.edu/~rogaway/ocb/news/code/ocb_ref.c) and [ruby test vector generation script](https://www.cs.ucdavis.edu/~rogaway/ocb/news/code/ocb_vectors.rb).

## Dealing with Bitrot

To run the ruby test vector generation script, I needed to pull the `ruby-aes` gem from [the web archive](https://web.archive.org/web/20140514180355/http://rubyforge.org/frs/download.php/30177/ruby-aes-1.1.tar.gz), since rubyforge is no more, and i didn't see the same gem on modern ruby sites.

The ruby script also made some assumptions about strings and bytes that don't hold for modern ruby, and formats its output slightly differently than the data in the RFC.

## Status

Help is welcome to complete anything unfinished!

- [x] generate [the first section of test vectors](test-vector-1.txt) (generated with Ruby, confirmed with OpenSSL)
- [x] parameterize section 1 (output with varying nonces: [120-bit](test-vector-1-nonce120.txt) [112-bit](test-vector-1-nonce112.txt) [104-bit](test-vector-1-nonce104.txt))
- [x] generate [the second section of test vectors](test-vector-2.txt) (generated with OpenSSL)
- [x] parameterize section 2 (output with varying nonces: [120-bit](test-vector-2-nonce120.txt) [112-bit](test-vector-2-nonce112.txt) [104-bit](test-vector-2-nonce104.txt))
- [x] generate [the third section of test vectors](test-vector-3.txt) (generated with OpenSSL)
- [x] parameterize section 3 (output with varying nonces: [120-bit](test-vector-3-nonce120.txt) [112-bit](test-vector-3-nonce112.txt) [104-bit](test-vector-3-nonce104.txt))


## Followups

Test vectors from this suite have been confirmed in the following codebases:

- [a prerelease version of nettle](https://gitlab.com/sequoia-pgp/nettle-rs/-/commit/f2d6d7f088c97a84dcfbdc8a4d10aad1efb0bee3)
- [python-cryptography](https://github.com/pyca/cryptography/pull/7009)
